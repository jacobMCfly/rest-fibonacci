from flask import Flask, render_template, make_response, jsonify, request
from flask_cors import CORS
import logging
import os

if os.path.exists("successLog.log"):
  logging.basicConfig(filename="successLog.log",level=logging.INFO)
else:
  f = open('successLog.log','x')
  f.close()
  logging.basicConfig(filename="successLog.log",level=logging.INFO)
 

REST_consumo_servicio = Flask(__name__)
CORS(REST_consumo_servicio, resources={r"*": {"origins": "*"}})

PORT = 3300
HOST = '0.0.0.0'

@REST_consumo_servicio.route('/')
def index() -> str:
    return render_template('index.html')

if __name__ == '__main__':
    print(f'Servidor de REST_consumo_servicio está corriendo en el puerto: {PORT}')
    REST_consumo_servicio.run(host=HOST, port=PORT)