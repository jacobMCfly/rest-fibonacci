from flask import Flask, render_template, make_response, jsonify, request
from flask_cors import CORS
from database.generarDB import database_init
import logging
import os
import sqlite3 


if os.path.exists("successLog.log"):
    logging.basicConfig(filename="successLog.log", level=logging.INFO)
else:
    f = open('successLog.log', 'x')
    f.close()
    logging.basicConfig(filename="successLog.log", level=logging.INFO)

REST_fibonacci = Flask(__name__)
CORS(REST_fibonacci, resources={r"*": {"origins": "*"}})

PORT = 3200
HOST = '0.0.0.0'

@REST_fibonacci.route('/obtener_numeros')
def obtener_numeros() -> str:
    con = sqlite3.connect('database/database.sqlite')
    con.row_factory = sqlite3.Row

    cur = con.cursor()
    cur.execute("SELECT * FROM numeros")

    rows = cur.fetchall()
    if rows:
        return make_response(jsonify([dict(ix) for ix in rows]), 200)


@REST_fibonacci.route('/guardar_numero', methods=['POST'])
def guardar_numero() -> str:
    req = request.get_json()
    if req['numero']:
        try:
            result = fibonacci(int(req['numero']))
            numero = int(req['numero'])
            resultado = result
            with sqlite3.connect('database/database.sqlite') as con:
                cur = con.cursor()
                cur.execute(
                    'INSERT INTO numeros (numero,resultado) VALUES (?,?)', (numero, str(resultado))
                )

                con.commit()
                res = make_response(
                    jsonify({'message': 'número agregado correctamente'}), 200)
        except:
            con.rollback()
            res = make_response(
                jsonify({'message': 'error en agregar el número'}), 400)
            return res
        finally:
            con.close()
            return res

    res = make_response(jsonify({'message': 'No se envió el número'}))
    return res


@REST_fibonacci.route('/eliminar_numero/<id>', methods=['DELETE'])
def eliminar_numero(id) -> str:
    if id:
        try:
            print(id)
            with sqlite3.connect('database/database.sqlite') as con:
                cur = con.cursor()
                cur.execute(
                    'DELETE FROM numeros WHERE id=?', (id,))

                con.commit()
                res = make_response(
                    jsonify({'message': 'número eliminado'}), 200)
        except:
            con.rollback()
            res = make_response(
                jsonify({'message': 'Error en la eliminación'}), 400)
            return res
        finally:
            con.close()
            return res
    res = make_response(jsonify({'message': 'id no encontrado'}), 400)
    return res


def fibonacci(n: int) -> int:
    f = [0, 1]
    for i in range(2, n+1):
        f.append(f[i-1] + f[i-2])

    return f[n]


if __name__ == '__main__':
    database_init()
    print(f'Servidor de REST_fibonacci está corriendo en el puerto:  {PORT}')
    REST_fibonacci.run(host=HOST, port=PORT)
