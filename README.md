## Instrucciones de instalación

#### Con Docker:

##### Requerimientos:
1. Python 3 https://www.python.org/downloads/
2. Docker https://docs.docker.com/get-docker/ 

Crear la imagen de Docker para cada proyecto, en este caso se configurará así, ya que la imagen no está en la nube:  
**el comando debe ejecutarse dentro de la carpeta de cada proyecto, ejemplo:  
dentro de rest_fibonacci**

```
$ docker build -t rest_fibonacci .
$ docker build -t rest_consumo_servicio .
```

Para correr la imagen de **rest_fibonacci**

```
$ docker run -it --name container-rest_fibonacci -p 3200:3200 rest_fibonacci
```
Para correr la imagen de **rest_consumo_servicio**

```
$ docker run -it --name container-rest_consumo_servicio -p 3300:3300 rest_consumo_servicio
```


#### Sin Docker:

##### Requerimientos:
1. Python 3 https://www.python.org/downloads/
3. Virtual Environment ```pip3 install virtualenv```  
  
Ejecutar el siguiente comando para crear el ambiente virtual:    

```
$ python3 -m venv env
```
Ejecutar ambiente virtual creado, después instalar dependencias del proyecto:
El archivo de *requirements.txt* está dentro del proyecto **rest_fibonacci**
```
$ pip3 install -r requirements.txt
$ python3 -m pip3 install --upgrade pip
```

Crear base de datos, dentro de la carpeta **rest_fibonacci/database**

```
$ python3 rest_fibonacci/database/generarDB.py
```

Correr el **primer** proyecto **rest_fibonacci**

```
$ python3 rest_fibonacci/REST_fibonacci.py
```

Correr el **segundo** proyecto **rest_fibonacci**

```
$ python3 rest_fibonacci/REST_consumo_servicio.py
```

## IMPORTANTE

La **REST** que calcula el número de fibonacci está en el puerto **3200**
La **REST** que consume el servicio está en el puerto **3300** 
 
**EN EL PUERTO 3300 ESTA UNA APLICACION EN VUEJS**

Nota.- **Los proyectos deben correrse en diferentes consolas**


## IMAGENES

![Imagen de vista general](./images/general.png)

![Imagen creando un número](./images/creado.png)

![Imagen eliminando un número](./images/alerta.png)
