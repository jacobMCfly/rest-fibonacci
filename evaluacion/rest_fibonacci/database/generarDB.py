import sqlite3

def database_init():
    try:
        conn = sqlite3.connect('REST_fibonacci/database/database.sqlite')
        print("Opened database successfully")
        conn.execute('CREATE TABLE IF NOT EXISTS numeros ('
                'id INTEGER PRIMARY KEY AUTOINCREMENT,'
                'numero INTEGER,'
                'resultado TEXT'
                ')'
                ) 
        conn.close()
    except:
        print("La base de datos existe")