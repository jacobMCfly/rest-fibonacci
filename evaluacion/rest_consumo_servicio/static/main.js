new Vue({
    el: '#app',
    vuetify: new Vuetify(),

    data() {
        return {
            number: undefined,
            headers: [
                {
                    text: 'id',
                    align: 'start',
                    sortable: true,
                    value: 'id',
                },
                { text: 'Numero', value: 'numero' },
                { text: 'Resultado', value: 'resultado' },
                { text: 'Opciones', value: 'opciones', align: 'right' }
            ],
            numeros: [],
            dialogConfirmarEliminar: false,
            dialogConfirmarActualizar: false,
            instanciaSeleccionada: undefined,
            reglasNumero: [
                value => !!value || 'Valor requerido',
                value => Number(value) > 0 || 'El número debe ser positivo',
                value => Number.isInteger(Number(value)) || "El número debe ser entero"
            ],
            formNumeroValido: false,
            snackBarStatus: 'info',
            requestMessage: '',
            snackBarVisible: false
        };
    },
    delimiters: ['{', '}'],

    mounted() {
        this.obtenerNumeros()
    },
    methods: {
        obtenerNumeros() {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.open('GET', 'http://127.0.0.1:3200/obtener_numeros')

            xmlhttp.send();

            xmlhttp.onload = () => {
                this.numeros = JSON.parse(xmlhttp.responseText)
                console.log(this.numeros)
                response = JSON.parse(xmlhttp.responseText)
                codigo = xmlhttp.status
                if (codigo === 400) {
                    this.snackBarStatus = 'error'
                    this.requestMessage = response.message
                    this.snackBarVisible = true
                }
            }
        },
        dialogEliminar(item) {
            this.instanciaSeleccionada = { ...item }
            this.dialogConfirmarEliminar = true
        },
        eliminarRegistro() {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.open('DELETE', `http://127.0.0.1:3200/eliminar_numero/${this.instanciaSeleccionada.id}`)

            xmlhttp.onload = () => {
                response = JSON.parse(xmlhttp.responseText)
                codigo = xmlhttp.status
                if (codigo === 200) {
                    this.snackBarStatus = 'success'
                    this.requestMessage = response.message
                    this.snackBarVisible = true
                    this.obtenerNumeros()
                    this.dialogConfirmarEliminar = false
                }
                if (codigo === 400) {
                    this.snackBarStatus = 'error'
                    this.requestMessage = response.message
                    this.snackBarVisible = true
                    this.obtenerNumeros()
                    this.dialogConfirmarEliminar = false
                }
            }  
            xmlhttp.send(null)  
        },
        agregarNumero() {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "http://127.0.0.1:3200/guardar_numero")

            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            xmlhttp.onload = () => {
                response = JSON.parse(xmlhttp.responseText)
                codigo = xmlhttp.status
                if (codigo === 200) {
                    this.snackBarStatus = 'success'
                    this.requestMessage = response.message
                    this.snackBarVisible = true
                    this.obtenerNumeros()
                }
                if (codigo === 400) {
                    this.snackBarStatus = 'error'
                    this.requestMessage = response.message
                    this.snackBarVisible = true
                    this.obtenerNumeros()
                }
            }

            let data = {
                numero: this.number,
            };

            let dataToJson = JSON.stringify(data)
            xmlhttp.send(dataToJson)
        }
    }
})